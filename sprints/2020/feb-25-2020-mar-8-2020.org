#+TITLE: February 25, 2020 - March 8, 2020 (13 days)
#+PROPERTY: Effort_ALL 0 0:05 0:10 0:30 1:00 2:00 3:00 4:00
#+COLUMNS: %35ITEM %TASKID %OWNER %3PRIORITY %TODO %5ESTIMATED{+} %3ACTUAL{+}
* REPORTS
** SCRUM BOARD
#+BEGIN: block-update-board
#+END:
** DEVELOPER SUMMARY
#+BEGIN: block-update-summary
#+END:
** BURNDOWN CHART
#+BEGIN: block-update-graph
#+END:
** BURNDOWN LIST
#+PLOT: title:"Burndown" ind:1 deps:(3 4) set:"term dumb" set:"xtics scale 0.5" set:"ytics scale 0.5" file:"burndown.plt" set:"xrange [0:17]"
#+BEGIN: block-update-burndown
#+END:
** TASK LIST
#+BEGIN: columnview :hlines 2 :maxlevel 5 :id "TASKS"
#+END:
* TASKS
  :PROPERTIES:
  :ID:       TASKS
  :SPRINTLENGTH: 13
  :SPRINTSTART: <2020-02-25 Tue>
  :wpd-akshay196: 1
  :wpd-sandeepk: 1
  :END:
** akshay196
*** DONE Introduction to Operating Systems - Part II
    CLOSED: [2020-03-08 Sun 17:49]
    :PROPERTIES:
    :ESTIMATED: 13
    :ACTUAL:   7.93
    :OWNER: akshay196
    :ID: READ.1580485531
    :TASKID: READ.1580485531
    :END:
    :LOGBOOK:
    CLOCK: [2020-03-08 Sun 16:23]--[2020-03-08 Sun 17:49] =>  1:26
    CLOCK: [2020-03-08 Sun 09:12]--[2020-03-08 Sun 09:26] =>  0:14
    CLOCK: [2020-03-08 Sun 07:23]--[2020-03-08 Sun 08:14] =>  0:51
    CLOCK: [2020-03-07 Sat 08:05]--[2020-03-07 Sat 09:15] =>  1:10
    CLOCK: [2020-03-06 Fri 07:10]--[2020-03-06 Fri 08:13] =>  1:03
    CLOCK: [2020-03-04 Wed 06:52]--[2020-03-04 Wed 07:26] =>  0:34
    CLOCK: [2020-03-03 Tue 07:09]--[2020-03-03 Tue 07:33] =>  0:24
    CLOCK: [2020-03-01 Sun 08:23]--[2020-03-01 Sun 09:40] =>  1:17
    CLOCK: [2020-02-28 Fri 21:09]--[2020-02-28 Fri 22:06] =>  0:57
    :END:
    https://classroom.udacity.com/courses/ud923
    - [X] Sample Midterm Questions              (180 min)
    - [X] Scheduling                            (240 min)
    - [X] Memory Management                     (180 min)

** sandeepk
*** DONE Write a blog post PART II [1/1]
    CLOSED: [2020-03-08 Sun 18:00]
    :PROPERTIES:
    :ESTIMATED: 7
    :ACTUAL:   3.33
    :OWNER: sandeepk
    :ID: WRITE.1581365835
    :TASKID: WRITE.1581365835
    :END:
    :LOGBOOK:
    CLOCK: [2020-03-08 Sun 17:00]--[2020-03-08 Sun 17:50] =>  0:50
    CLOCK: [2020-03-06 Fri 23:05]--[2020-03-06 Fri 23:50] =>  0:45
    CLOCK: [2020-03-05 Thu 23:10]--[2020-03-05 Thu 23:55] =>  0:45
    CLOCK: [2020-03-04 Wed 22:00]--[2020-03-04 Wed 23:00] =>  1:00
    :END:
    - [X] First Class Function blog post ( 7 hr )
*** DONE Book Atomic Habits Reading PART II [9/9]
    CLOSED: [2020-03-07 Sat 18:00]
    :PROPERTIES:
    :ESTIMATED: 6
    :ACTUAL:   8.17
    :OWNER: sandeepk
    :ID: READ.1581364101
    :TASKID: READ.1581364101
    :END:
    :LOGBOOK:
    CLOCK: [2020-03-07 Sat 17:05]--[2020-03-07 Sat 18:00] =>  0:55
    CLOCK: [2020-03-07 Sat 14:00]--[2020-03-07 Sat 15:50] =>  1:50
    CLOCK: [2020-03-06 Fri 10:05]--[2020-03-06 Fri 10:30] =>  0:25
    CLOCK: [2020-03-05 Thu 10:05]--[2020-03-05 Thu 10:35] =>  0:30
    CLOCK: [2020-03-04 Wed 10:00]--[2020-03-04 Wed 10:30] =>  0:30
    CLOCK: [2020-03-03 Tue 22:00]--[2020-03-03 Tue 22:20] =>  0:20
    CLOCK: [2020-03-03 Tue 09:55]--[2020-03-03 Tue 10:30] =>  0:35
    CLOCK: [2020-03-02 Mon 10:05]--[2020-03-02 Mon 10:35] =>  0:30
    CLOCK: [2020-03-01 Sun 23:30]--[2020-03-02 Mon 00:00] =>  0:30
    CLOCK: [2020-02-29 Sat 10:00]--[2020-02-29 Sat 10:30] =>  0:30
    CLOCK: [2020-02-28 Fri 10:10]--[2020-02-28 Fri 10:30] =>  0:20
    CLOCK: [2020-02-27 Thu 10:05]--[2020-02-27 Thu 10:30] =>  0:25
    CLOCK: [2020-02-26 Wed 10:05]--[2020-02-26 Wed 10:30] =>  0:25
    CLOCK: [2020-02-25 Tue 10:00]--[2020-02-25 Tue 10:25] =>  0:25
    :END:
    - [X] The Law of least effort                                        ( 40 min )
    - [X] How to stop procastinating by using the two minute rule        ( 30 min )
    - [X] How to make Good Habits Inevitable and Bad Habits Impossible   ( 30 min )
    - [X] The Cardinal Rule of Behaviour Change                          ( 30 min )
    - [X] How to stick with Good Habits Every Day                        ( 40 min )
    - [X] How an Accountablitiy Partner Can Change Everthing             ( 60 min )
    - [X] The turth about talent                                         ( 70 min )
    - [X] The Goldilocks Rule: How to staty motivated in life and work   ( 30 min )
    - [X] The downside of creating good habits                           ( 30 min )

      
